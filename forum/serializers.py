from rest_framework import serializers

from .models import Board, Category, Topic

class BoardListSerializer(serializers.ModelSerializer):
    # name = BoardListSerializer(read_only=True)

    class Meta:
        model = Board
        fields = ('id', 'title', 'slug',)

class BoardCreateSerializer(serializers.ModelSerializer):
    # name = BoardListSerializer(read_only=True)

    class Meta:
        model = Board
        fields = ('id', 'title', 'name',)

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ("id", 'name',)

class CategoryListSerializer(serializers.ModelSerializer):
    board_name = BoardListSerializer(many=True, read_only=True)
    class Meta:
        model = Category
        fields = ("id", 'name', 'board_name',)

class CategoryCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('name',)

class TopicListSerializer(serializers.ModelSerializer):
    created_by = serializers.StringRelatedField(read_only=True)
    board = serializers.StringRelatedField(read_only=True)
    
    class Meta:
        model = Topic
        fields = ['id', 'title', 'slug', 'board', 'created_by']

class BoardSerializer(serializers.ModelSerializer):
    topic = TopicListSerializer(many=True, read_only=True)

    class Meta:
        model = Topic
        fields = ['id', 'title', 'topic' ]

class TopicCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Topic
        fields = ['id', 'title', 'board', 'created_by']


class BoradRetriveSerializer(serializers.ModelSerializer):
    topic = TopicListSerializer(many=True, read_only=True)

    class Meta:
        model = Topic
        fields = ['id', 'title', 'topic' ]