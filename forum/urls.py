from django.urls import path

from .api import (
        CategoryAPI,
        CategoryCreateAPI,
        CategoryDeleteAPI,
        BoardAPI,
        BoardListAPI,
        BoardCreateAPI,
        BoardDeleteAPI,
        BoardRetriveAPI,
        TopicListAPI,
        TopicCreateAPI,
        TopicDeleteAPI,
    )        

urlpatterns = [
    path('api/category/', CategoryAPI.as_view(), name='category_list'),
    path('api/category/add/', CategoryCreateAPI.as_view(), name='add_category'),
    path('api/category/<id>/', CategoryDeleteAPI.as_view(), name='delete_category'),
    path('api/board/', BoardAPI.as_view(), name='board_api'),
    # path('api/board/', BoardListAPI.as_view(), name='board_list'),
    path('api/board/add/', BoardCreateAPI.as_view(), name='add_board'),
    path('api/board/<slug>/', BoardRetriveAPI.as_view(), name='retrive_board'),
    path('api/board/update/<slug>/', BoardDeleteAPI.as_view(), name='delete_board'),
    path('api/topic/', TopicListAPI.as_view(), name='topic_api'),
    path('api/topic/add/', TopicCreateAPI.as_view(), name='topic_add'),
    path('api/topic/<slug>/', TopicDeleteAPI.as_view(), name='topic_delete'),

]