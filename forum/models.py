from django.db import models
from django.db.models.signals import pre_save
from django_extensions.db.fields import AutoSlugField

from accounts.models import DateTimeModel
from .utils import slug_creator

class PostManager(models.Manager):
    def all(self, *args, **kwargs):
        return super(PostManager, self).filter(deleted_at__isnull=True)

class Category(DateTimeModel):
    name = models.CharField(max_length=50)

    objects = PostManager()

   
    def __str__(self):
        return self.name

class Board(DateTimeModel):
    name = models.ForeignKey(Category, related_name='board_name', on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    slug = models.SlugField(unique=True)
    
    objects = PostManager()

    class Meta:
        ordering = ['-created_at',]
        verbose_name = 'Board'

    def __str__(self):
        return self.title

class Topic(DateTimeModel):
    title = models.CharField(max_length=255)
    slug = AutoSlugField(unique=True,populate_from='title')
    board = models.ForeignKey(Board, related_name='topic', on_delete=models.CASCADE)
    created_by = models.ForeignKey("accounts.User", related_name='topic_created_by', on_delete=models.CASCADE)

    objects = PostManager()

    class Meta:
        ordering = ['created_at']
        verbose_name = 'Topic'

    def __str__(self):
        return self.title
    

class Post(DateTimeModel):
    message = models.TextField(max_length=5000)
    topic = models.ForeignKey(Topic, related_name='posts', on_delete=models.CASCADE)
    crreated_by = models.ForeignKey("accounts.User", related_name="post_created_by", on_delete=models.CASCADE)

    class Meta:
        ordering = ['created_at']
        verbose_name = 'Post'

    def __str__(self):
        return self.topic
    

def create_slug(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = slug_creator(instance)

pre_save.connect(create_slug, sender=Board)