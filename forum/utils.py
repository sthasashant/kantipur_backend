from django.utils.text import slugify

def slug_creator(instance, new_slug=None):
    slug = slugify(instance.title)
    if new_slug is not None:
        slug = new_slug

    #Provide the model class of the instance
    klass = instance.__class__
    qs = klass.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" %(instance.title, qs.first().id)
        return slug_creator(instance, new_slug=new_slug)
    return slug
