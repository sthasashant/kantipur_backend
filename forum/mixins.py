from django.utils import timezone

from rest_framework import status
from rest_framework.generics import (
    RetrieveUpdateDestroyAPIView,
    CreateAPIView
)

from rest_framework.response import Response

class CreateAPIMixin(CreateAPIView):
    def post(self, request, *args, **kwargs):
        self.create(request, *args, **kwargs)
        message = {
            'status': True,
            'state': 'Success',
            'detail': 'Created Successfully'

        }
        return Response(status=status.HTTP_201_CREATED, data=message)

class RetrieveUpdateDeleteMixin(RetrieveUpdateDestroyAPIView):
    def delete(self, request, *args, **kwargs):
        try:
            obj = self.get_object()
            obj.deleted_at = timezone.now()
            obj.save()
            message = {
                'status': True,
                'state': 'Success',
                'detail': 'Deleted Successfully'
            }
            return Response(status=status.HTTP_204_NO_CONTENT, data=message )
        
        except Exception as e:
            return e