from rest_framework import  status
from rest_framework.generics import (
      ListAPIView,
      CreateAPIView,
      RetrieveAPIView
   )
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from .models import Category, Board, Topic
from .serializers import (
      CategorySerializer,
      CategoryCreateSerializer,
      CategoryListSerializer,
      BoardSerializer,
      BoardListSerializer,
      BoardCreateSerializer,
      BoradRetriveSerializer,
      TopicListSerializer,
      TopicCreateSerializer,
)

from .mixins import (
    RetrieveUpdateDeleteMixin,
    CreateAPIMixin
)

class CategoryAPI(ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategoryListSerializer
    permission_classes = [AllowAny,]

class CategoryCreateAPI(CreateAPIMixin):
    queryset = Category.objects.all()
    serializer_class = CategoryCreateSerializer
    permission_classes = [AllowAny,]
    
class CategoryDeleteAPI(RetrieveUpdateDeleteMixin):
    queryset = Category.objects.all()
    serializer_class = CategoryCreateSerializer
    permission_classes = [AllowAny,]
    lookup_field = 'id'

class BoardAPI(ListAPIView):
    queryset = Board.objects.all()
    serializer_class = BoardSerializer
    permission_classes = [AllowAny, ]

class BoardListAPI(ListAPIView):
    queryset = Board.objects.filter(deleted_at__isnull=True)
    serializer_class = BoardListSerializer
    permission_classes = [AllowAny,]


class BoardCreateAPI(CreateAPIMixin):
    queryset = Board.objects.filter(deleted_at__isnull=True)
    serializer_class = BoardCreateSerializer
    permission_classes = [AllowAny,]

class BoardDeleteAPI(RetrieveUpdateDeleteMixin):
    queryset = Board.objects.all()
    serializer_class = BoardCreateSerializer
    permission_classes = [AllowAny,]
    lookup_field = 'slug'

class BoardRetriveAPI(ListAPIView):
    queryset = Board.objects.all()
    serializer_class = BoradRetriveSerializer
    permission_classes = [AllowAny,]
    lookup_field = 'slug'

    def list(self, request, slug, *args, **kwargs):
        queryset = Board.objects.filter(slug=slug)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class TopicListAPI(ListAPIView):
    queryset = Topic.objects.all()
    serializer_class = TopicListSerializer
    permission_classes = [AllowAny,]

class TopicCreateAPI(CreateAPIMixin):
    queryset = Topic.objects.all()
    serializer_class = TopicCreateSerializer
    permission_classes = [AllowAny,]

class TopicDeleteAPI(RetrieveUpdateDeleteMixin):
    queryset = Topic.objects.all()
    serializer_class = TopicCreateSerializer
    permission_classes = [AllowAny,]
    lookup_field = 'slug'
