from rest_framework import status
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import UserLoginSerializer

class UserLoginAPIView(APIView):
    # permissions_classes = [IsAuthenticated]
    authentication_classes = (SessionAuthentication,)
    serializer_class = UserLoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = UserLoginSerializer(data=request.data)
        print(serializer.is_valid(),'sdsad')
        if serializer.is_valid():
            new_data = serializer.data
            username = new_data["username"]
            password = new_data['password']
            print(new_data)
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
            else:
                return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
            return Response(new_data, status=HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
