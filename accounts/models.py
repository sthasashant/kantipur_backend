from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone

class DateTimeModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False, )
    updated_at = models.DateTimeField(auto_now_add=False, auto_now=True, )
    deleted_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        abstract = True

    def delete(self, *args, **kwargs):
        self.deleted_at = timezone.now()
        super().save()

class User(AbstractUser, DateTimeModel):
    address = models.CharField(max_length=255, null=True, blank=True)
    phone_number = models.CharField(max_length=32, null=True, blank=True)
    dob = models.DateTimeField(null=True, blank=True)
    # photo = models.ImageField(upload_to="profile_pics/", default="default.jpg")
    
    class Meta:
        ordering = ["-created_at", ]
        verbose_name = "User"
        verbose_name_plural = "Users"

    def __str__(self):
        return self.username


